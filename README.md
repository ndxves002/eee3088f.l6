# EEE3088F.L6

Pi HAT Description
The microHAT designed by group 24 is a temperature and light sensing microHAT. The HAT contains a LDR as well as a thermistor, thus giving it the capabilities to accurately record the light intensity and temperature in its environment. This adds a great deal of functionality to the Pi where it can monitor or control an environment which is sensitive to either temperature or light. Examples of such scenarios include terrariums, greenhouses or similar housing for living organisms where the Pi could receive feedback from the HAT regarding the environmental parameters and display them, send them to a user or activate processes to control and correct the parameters. Similarly they could be used to monitor and the temperature of any temperature dependent equipment as many electronics are, the Pi could use the information to alert users of overheating or activate cooling systems when necessary. 

Pi HAT Intended usage
-Animal inclosure (eg. snake habitat)
-Computer protection and user safety.
-Indoor Plant Control

List of Materials
-Wire
-Voltage source
-Op Amps
-LDR
-thermistor
-resistor
-capicitors
-DAC IC
-Diodes

How to Contribute
Add code for secrion functioning
